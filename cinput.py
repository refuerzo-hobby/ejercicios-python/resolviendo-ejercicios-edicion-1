import sys

__all__ = ['cinput']
def _autocast(block, cast=None):
    if cast is None:
        cast = [str, bool, float, int, list, tuple, dict, set]
    try:
        print(cast[-1])
        # Intentar convertir el bloque al tipo actual en la lista
        if cast[-1] is bool:
            # Si el tipo es bool, manejar casos especiales para cadenas que representan valores booleanos
            lower_block = block.lower()
            if lower_block == 'true' or lower_block == '1':
                return True
            elif lower_block == 'false' or lower_block == '0':
                return False
            else:
                # Si no es 'true' o 'false', dejar que el bloque sea procesado normalmente
                raise ValueError
        if cast[-1] in [dict, list, tuple]:
            if '(' in block and ')' in block and cast[-1] in [tuple]:
                try:
                    return eval(block)
                except SyntaxError:
                    raise ValueError()
            if '[' in block and ']' in block and cast[-1] in [list]:
                try:
                    return eval(block)
                except SyntaxError:
                    raise ValueError()
            if '{' in block and '}' in block and cast[-1] in [dict]:
                try:
                    return eval(block)
                except SyntaxError:
                    raise ValueError()
            if '{' in block and '}' in block and cast[-1] in [set]:
                try:
                    return eval(block)
                except SyntaxError:
                    raise ValueError()

            raise ValueError()
        else:
            casted_block = cast[-1](block)
            return casted_block
    except (ValueError, TypeError):
        # Si la conversión falla, intentar con los tipos restantes en la lista
        if len(cast) > 1:
            return _autocast(block, cast[:-1])
        else:
            # Si no hay más tipos para probar, devolver el bloque original
            return block


def cinput(prompt, end=': ', data_type=None, autocast=False):
    """
    Función que muestra un prompt personalizado y lee una línea de la entrada estándar.

    :param prompt: Mensaje que se muestra como prompt.
    :param end: Sufijo que se muestra al final del prompt.
    :return: La línea ingresada por el usuario, sin el carácter de nueva línea al final.
    """

    sys.stdout.write(prompt + end)
    capture = sys.stdin.readline().strip()

    if data_type is None and not autocast:
        return capture

    if autocast:
        return _autocast(capture)

    try:
        return data_type(capture)
    except ValueError:
        print(f"Error: No se pudo convertir la entrada a {data_type.__name__}. Se devolverá como cadena.")
        return capture


if __name__ == '__main__':
    while True:
        # print(cinput('Ingresa un Texto'))
        # print(cinput('Ingresa un Numero', data_type=int))
        print(cinput('Ingresa Cualquier Cosa', autocast=True))
