import os
import importlib


def execute(archivo):
    try:
        # Cargar el módulo dinámicamente
        modulo = importlib.import_module(archivo[:-3].replace('\\', '.').replace('/', '.'))  # Eliminar la extensión .py
        # Obtener todas las funciones del módulo
        funciones = [nombre for nombre in dir(modulo) if callable(getattr(modulo, nombre))]
        # Ejecutar todas las funciones "run" en el módulo
        for funcion_nombre in funciones:
            if funcion_nombre.startswith("run") or funcion_nombre.startswith("info"):
                funcion = getattr(modulo, funcion_nombre)
                funcion()

    except Exception as e:
        print(f"Error al ejecutar funciones en el archivo {archivo}: {e}")


def imprimir_archivos(archivos, pagina_actual, archivos_por_pagina=10):
    inicio = (pagina_actual - 1) * archivos_por_pagina
    fin = inicio + archivos_por_pagina

    print(
        f"Archivos disponibles para ejecutar (Página {pagina_actual}/{int(len(archivos) / archivos_por_pagina) + 1}):")
    for i, archivo in enumerate(archivos[inicio:fin], start=inicio + 1):
        print(f"{i}. {archivo}")


def paginar(archivos, archivos_por_pagina=10):
    paginas = (len(archivos) + archivos_por_pagina - 1) // archivos_por_pagina
    pagina_actual = 1

    while True:
        print('*' * 100)
        imprimir_archivos(archivos, pagina_actual, archivos_por_pagina)
        print('*' * 100)
        opcion = input(
            f"Ingrese 'n' para la siguiente página, 'p' para la página anterior,"
            f"\no seleccione el número del archivo que desea ejecutar: ")

        if opcion.lower() == 'n':
            if pagina_actual < paginas:
                pagina_actual += 1
            else:
                print("Ya está en la última página.")
        elif opcion.lower() == 'p':
            if pagina_actual > 1:
                pagina_actual -= 1
            else:
                print("Ya está en la primera página.")
        elif opcion.isdigit():
            seleccion = int(opcion)
            if 1 <= seleccion <= len(archivos):
                return archivos[seleccion - 1]
            else:
                print("Por favor, seleccione un número válido.")
        else:
            print("Entrada no válida. Intente nuevamente.")


def menu_principal():
    """

    :return:
    """
    print("=== Menú Principal ===",
          "1. Ejecutar programas",
          "2. Configuración",
          "3. Salir", sep='\n')
    opcion = input("Seleccione una opción (1, 2, 3): ")
    return opcion


def menu_exec(directorio):
    """

    :param directorio:
    :return:
    """
    excludes = ['__init__.py']

    try:
        archivos = [archivo for archivo in os.listdir(directorio) if
                    archivo.endswith(".py") and archivo not in excludes]
        if not archivos:
            print("No hay archivos disponibles para ejecutar.")
            return

        execute(os.path.join(directorio, paginar(archivos)))

    except Exception as e:
        print(f"Error al ejecutar funciones en el directorio {directorio}: {e}")


if __name__ == '__main__':
    directorio_src = "src"

    while True:
        opcion = menu_principal()

        if opcion == '1':
            menu_exec(directorio_src)
        elif opcion == '2':
            print("Configuración aún no implementada.")
        elif opcion == '3':
            print("Finalizando programa. ¡Hasta luego!")
            break
        else:
            print("Opción no válida. Intente nuevamente.")
