"""
EXERCISE #003

Cree una función que retorne el número de palabras presentes en un string
(obs: considere que toda palabra válida está separada por un espacio de
la anterior).
"""


def exercise(block):
    return len(block.split())


def run():
    print(exercise(input('Ingrese una cadena de texto: ')))


if __name__ == '__main__':
    run()
