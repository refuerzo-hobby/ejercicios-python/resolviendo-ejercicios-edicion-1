"""
EXERCISE #003

Cree una función que reciba un string y retorne el string invertido.
"""


def exercise(block):
    return block[::-1]


def run():
    print(exercise(input('Ingrese una cadena de texto: ')))


if __name__ == '__main__':
    run()
