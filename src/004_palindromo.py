"""
EXERCISE #003

Cree una función que reciba un string s y retorne True ssi s es un
palíndromo (una palabra que se lee igual en ambos sentidos, sin considerar
espacios).
"""


def exercise(block):
    org = block
    inv = block[::-1]

    if org.replace(' ', '') == inv.replace(' ', ''):
        return True

    return False


def run():
    print(exercise(input('Ingrese una cadena de texto: ')))


if __name__ == '__main__':
    run()
