"""
EXERCISE #002

Cree una funcion que reciba un string y dos enteros i, j, tal que i <= j; y
retorne la sub-parte del string que comienza en i y termina en j-1.
"""


def exercise(block, start, end):
    if start <= end:
        return block[start:end]
    return f"Error {start} > {end}"


def run():
    block = input('Ingrese la cadena a cortar: ')
    start = int(input('Posicion inicial de corte: '))
    end = int(input('Posicion final de corte: '))
    print(exercise(block, start, end))


if __name__ == '__main__':
    info()
    run()
