"""
EXERCISE #

Cree una función que reciba un string y retorne el mismo string, pero sin
los caracteres pares.
"""


def exercise(block):
    """
        docstring
    """
    return block[::2]


def run():
    """
        docstring
    """
    block = input('Ingrese una cadena de texto: ')
    result = exercise(block)
    print(result)


if __name__ == '__main__':
    info()
    run()
