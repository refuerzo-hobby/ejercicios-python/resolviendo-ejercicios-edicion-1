"""
EXERCISE #003

Cree un programa que pida párrafos al usuario hasta que él ingrese un
`-1'. Guarde los párrafos en un string (considerando saltos de línea). Al
Fínalizar el programa, muestre al usuario el texto completo ingresado.
"""


def exercise():
    lines: list = []
    inp = input('Escribiremos un parrafo, ingresa una linea: ')

    while inp != '-1':
        lines.append(inp)
        inp = input('Ingresa una nueva linea, para terminar escribe "-1": ')

    for line in lines:
        print(line)


def run():
    exercise()


if __name__ == '__main__':
    run()
