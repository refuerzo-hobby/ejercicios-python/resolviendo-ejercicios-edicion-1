"""
EXERCISE #003

Cree una función que resuelve el Capicúa usando strings.
"""


def exercise(block):
    org = block
    inv = block[::-1]

    if org == inv:
        return True

    return False


def run():
    print(exercise(input('Ingrese una cadena de texto: ')))


if __name__ == '__main__':
    run()
