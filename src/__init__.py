"""
EXERCISE #


"""


def exercise():
    pass

def info():
    print('*' * 50)
    print(
        f'Title: String en Python (parte 1)',
        f'Author: Rodrigo Toro Icarte',
        f'Pages: 4',
        f'Exercises: 7',
        sep='\n'
    )
    print('*' * 50)


def run():
    exercise()


if __name__ == '__main__':
    info()
    run()
